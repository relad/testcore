Локально используемая версия Python: **3.8.10**

Локально используемая версия redis-server: **6.2.7**

Локально используемая ОС: macOS Monterey **12.3**

Минимальная требуемая версия Python: **3.7.5**

---
### Запуск проекта
```shell
git clone https://gitlab.com/relad/testcore.git
```
```shell
cd testcore
```
```shell
python3 -m venv env
```
```shell
source env/bin/activate
```
```shell
pip install -r requirements.txt
```
```shell
cd src
```
```shell
touch .env

# Создание файла для хранения переменных окружения
# Необходимо создать след. переменные окружения: SECRET_KEY, DEBUG, ALLOWED_HOSTS, CELERY_BROKER_URL, CELERY_RESULT_BACKEND, ORDER_SENDING_URL
```
```shell
python manage.py makemigrations
```
```shell
python manage.py migrate
```
```shell
python manage.py runserver
```
NOTE: команда для создания суперпользователя
```shell
python manage.py createsuperuser
```

---
### Запуск Celery:
```shell
python -m celery -A core worker -l INFO
```
