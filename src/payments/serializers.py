from rest_framework import serializers

from .models import Payment


class PaymentCreateSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        payment = Payment.objects.create(**validated_data, amount=validated_data['order'].amount)
        return payment

    class Meta:
        model = Payment
        fields = ('id', 'order', 'amount', 'payment_type', 'status')
        read_only_fields = ('id', 'amount', 'status')
