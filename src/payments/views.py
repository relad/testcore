from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin

from .models import Payment
from .serializers import PaymentCreateSerializer


class PaymentViewSet(CreateModelMixin, GenericViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentCreateSerializer
