from django.db import models
from django.utils.translation import gettext_lazy as _, gettext


class Payment(models.Model):
    IN_PROGRESS, REJECTED, PAID = 'in_progress', 'rejected', 'paid'
    CREDIT_CARD, CASH = 'credit_card', 'cash'
    STATUS_CHOICES = (
        (IN_PROGRESS, _('В ходе выполнения')),
        (REJECTED, _('Отклонен')),
        (PAID, _('Оплачен'))
    )
    PAYMENT_TYPE_CHOICES = (
        (CREDIT_CARD, _('Кредитная карта')),
        (CASH, _('Наличные'))
    )
    order = models.ForeignKey(
        'orders.Order',
        on_delete=models.CASCADE,
        related_name='payments',
        verbose_name=_('заказ'),
        limit_choices_to=~models.Q(status='confirmed')
    )
    amount = models.DecimalField(
        verbose_name=_('сумма'),
        max_digits=8,
        decimal_places=2
    )
    payment_type = models.CharField(
        verbose_name=_('тип оплаты'),
        max_length=32,
        default=CASH,
        choices=PAYMENT_TYPE_CHOICES
    )
    status = models.CharField(
        verbose_name=_('статус платежа'),
        max_length=16,
        default=IN_PROGRESS,
        choices=STATUS_CHOICES
    )

    def __str__(self):
        return gettext(f'{self.amount} | {self.status}')

    def save(self, *args, **kwargs):
        if not self.amount:
            self.amount = self.order.amount
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('платеж')
        verbose_name_plural = _('платежи')
