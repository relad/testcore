from django.contrib import admin

from .models import Payment


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'amount', 'payment_type', 'status')
    list_filter = ('payment_type', 'status')
    list_select_related = ('order',)
    raw_id_fields = ('order',)
    readonly_fields = ('amount',)
    show_full_result_count = False
