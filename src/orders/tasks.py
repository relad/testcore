from logging import getLogger

from django.conf import settings
from django.db.models import OuterRef, Subquery
from celery import shared_task
import requests

from payments.models import Payment    # import other app
from .models import Order

logger = getLogger('orders')


@shared_task(bind=True, default_retry_delay=300, max_retries=3)
def send_order(self, order_id):
    try:
        order = Order.objects.filter(pk=order_id).annotate(
            payment_amount=Subquery(
                Payment.objects.filter(order_id=OuterRef('pk'), status=Payment.PAID).values_list('amount')[:1]
            )
        ).values('id', 'payment_amount', 'confirm_at').first()
        response = requests.post(
            url=settings.ORDER_SENDING_URL,
            json={'id': order.get('id'), 'amount': str(order.get('payment_amount')), 'date': str(order.get('confirm_at'))},
            headers={'accept': 'application/json', 'Content-Type': 'application/json'}
        )
        if response.ok:
            logger.info(f'Successful to send order with id {order_id} to {settings.ORDER_SENDING_URL}')
        else:
            logger.warning(f'Failed to send order with id {order_id} to {settings.ORDER_SENDING_URL}\nServer response: {response.json()}')
    except Exception as error:
        logger.error('Error while sending order', exc_info=error)
        self.retry(error)
