from django.contrib import admin
from django.db.models import Exists, OuterRef
from django.shortcuts import redirect
from django.urls import reverse, path
from django.utils.safestring import mark_safe
from django.utils import timezone

from payments.models import Payment    # import other app
from .models import Order, OrderItem
from .tasks import send_order


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    fk_name = 'order'
    fields = ('product', 'quantity', 'current_price')
    readonly_fields = ('current_price', )
    autocomplete_fields = ('product',)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('product')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'amount', 'created_at', 'confirm_at', 'confirm_action')
    list_display_links = ('id', 'status')
    list_filter = ('status',)
    list_per_page = 15
    inlines = (OrderItemInline,)
    readonly_fields = ('status', 'confirm_at',)
    show_full_result_count = False

    def confirm_action(self, order):
        if order.status != order.CONFIRMED and order.has_success_payment:
            url = reverse('admin:confirm_action_url', kwargs={'pk': order.pk})
            return mark_safe(f'<a class="button" href="{url}">Подтвердить заказ</a>')
        return '-'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [path('confirm/<int:pk>', self.confirmation_callback, name='confirm_action_url')]
        return urls + custom_urls

    def confirmation_callback(self, request, pk):
        Order.objects.filter(pk=pk).update(status=Order.CONFIRMED, confirm_at=timezone.now())
        send_order.delay(pk)
        redirect_url = "admin:{}_{}_changelist".format(self.opts.app_label, self.opts.model_name)
        return redirect(reverse(redirect_url))

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.resolver_match.url_name.endswith('_change'):
            return queryset.annotate(
                has_success_payment=Exists(Payment.objects.filter(order_id=OuterRef('pk'), status=Payment.PAID))
            )
        return queryset.prefetch_related('items')

    confirm_action.short_description = 'Действие'
    confirm_action.allow_tags = True
