from rest_framework import serializers

from .models import Order, OrderItem


class OrderItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('id', 'product', 'current_price', 'quantity', 'total_price')
        read_only_fields = ('id', 'current_price', 'total_price')


class OrderCreateSerializer(serializers.ModelSerializer):
    items = OrderItemCreateSerializer(many=True, required=True)

    def create(self, validated_data):
        items = validated_data.pop('items', [])
        order = Order.objects.create(**validated_data)
        OrderItem.objects.bulk_create(
            [OrderItem(order=order, product=item['product'], quantity=item['quantity'], current_price=item['product'].price) for item in items]
        )
        return order

    class Meta:
        model = Order
        fields = ('id', 'status', 'items', 'created_at', 'confirm_at')
        read_only_fields = ('id', 'status', 'created_at', 'confirm_at')
