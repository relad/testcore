from django.db import models
from django.utils.translation import gettext_lazy as _, gettext


class OrderManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            _amount=models.Sum(
                models.F('items__current_price') * models.F('items__quantity'), output_field=models.DecimalField(max_digits=8, decimal_places=2)
            )
        )


class OrderItemManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            _total_price=models.Sum(models.F('quantity') * models.F('current_price'), output_field=models.DecimalField(max_digits=8, decimal_places=2))
        )


class Order(models.Model):
    IN_PROGRESS, REJECTED, CONFIRMED = 'in_progress', 'rejected', 'confirmed'
    STATUS_CHOICES = (
        (IN_PROGRESS, _('В ходе выполнения')),
        (REJECTED, _('Отклонен')),
        (CONFIRMED, _('Подтвержден'))
    )
    status = models.CharField(
        verbose_name=_('статус заказа'),
        max_length=16,
        default=IN_PROGRESS,
        choices=STATUS_CHOICES
    )
    created_at = models.DateTimeField(
        verbose_name=_('дата и время создания'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('дата и время обновления'),
        auto_now=True
    )
    confirm_at = models.DateTimeField(
        verbose_name=_('дата и время подтверждения заказа'),
        blank=True,
        null=True
    )
    objects = OrderManager()

    def __amount(self):
        if hasattr(self, '_amount'):
            return self._amount
        return sum([item for item in self.items.values_list('_total_price', flat=True)])

    def __str__(self):
        return f'{self.pk}'

    __amount.short_description = _('сумма')
    amount = property(__amount)

    class Meta:
        verbose_name = _('заказ')
        verbose_name_plural = _('заказы')


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='items',
        verbose_name=_('заказ')
    )
    product = models.ForeignKey(
        'products.Product',
        on_delete=models.SET_NULL,
        related_name='order_items',
        verbose_name=_('товар'),
        null=True
    )
    current_price = models.DecimalField(
        verbose_name=_('цена товара при покупке'),
        max_digits=8,
        decimal_places=2,
        blank=True
    )
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('количество товаров'),
        default=1
    )
    objects = OrderItemManager()

    def __total_price(self):
        if hasattr(self, '_total_price'):
            return self._total_price
        return self.current_price * self.quantity if self.current_price else 0

    def save(self, *args, **kwargs):
        if not self.current_price:
            self.current_price = self.product.price
        return super().save(*args, **kwargs)

    def __str__(self):
        return gettext('')

    __total_price.short_description = _('цена с учетом количества товаров')
    total_price = property(__total_price)

    class Meta:
        verbose_name = _('элемент заказа')
        verbose_name_plural = _('элементы заказов')
        constraints = (models.UniqueConstraint(fields=('order', 'product'), name='order_product_once'),)
