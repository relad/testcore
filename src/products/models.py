from django.db import models
from django.utils.translation import gettext_lazy as _, gettext


class Product(models.Model):
    name = models.CharField(
        verbose_name=_('наименование'),
        max_length=255
    )
    image = models.ImageField(
        verbose_name=_('иллюстративное изображение товара'),
        default='',
        blank=True,
        upload_to='images/products/%Y/%m/%d/'
    )
    content = models.TextField(
        verbose_name=_('контент'),
        default='',
        blank=True
    )
    price = models.DecimalField(
        verbose_name=_('цена'),
        max_digits=8,
        decimal_places=2
    )
    created_at = models.DateTimeField(
        verbose_name=_('дата и время создания'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('дата и время обновления'),
        auto_now=True
    )

    def __str__(self):
        return gettext(self.name)

    class Meta:
        verbose_name = _('товар')
        verbose_name_plural = _('товары')
