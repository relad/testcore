from utilities.serializers.serializers import ReadOnlyModelSerializer
from .models import Product


class ProductListSerializer(ReadOnlyModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'image', 'price')
