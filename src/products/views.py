from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin

from .models import Product
from .serializers import ProductListSerializer


class ProductViewSet(ListModelMixin, GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
